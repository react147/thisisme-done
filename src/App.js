import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="left">
        <img src="tka.jpg" alt="Timo"/>
      </div>
      <div className="right">
        <p>Timo Kankaanpää</p>
        <p>yliopettaja</p>
      </div>
    </div>
  );
}

export default App;
